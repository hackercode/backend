import app from "./app";
import knex from "./db";

const port = 3000;

app.listen(port, () => {
  console.log(`running on port ${port}`);
});

const init = async () => {
  try {
    const res = await knex.raw("SELECT NOW()");
    console.log(res);
  } catch (error) {
    console.log(error);
  }
};

init();
