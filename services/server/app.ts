import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import { QuestionRouter } from "./routes/index.routes";

dotenv.config();

const app = express();
app.set("trust proxy", true);
app.use(cors());
app.use(express.json());

app.use("/question", QuestionRouter);

export default app;
