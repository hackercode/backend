import knexConnection from "knex";

const host = process.env.DB_HOST;
const database = process.env.DB_NAME || "";
const password = process.env.DB_PASSWORD;

const knex = knexConnection({
  client: "pg",
  version: "13.7",
  connection: {
    host,
    user: "postgres",
    password,
    database,
    port: 5432,
  },
});

export default knex;
