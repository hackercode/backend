import lambdaApi from "lambda-api";
import { APIGatewayEvent, Context } from "aws-lambda";
import { Client } from "pg";
import { SecretsManager } from "aws-sdk";
import knexConnection from "knex";

const api = lambdaApi();

api.get("/question", (req, res) => {
  return {
    statusCode: 200,
    body: JSON.stringify({
      questions: [
        {
          id: 1,
          name: "Two sum 2",
        },
        {
          id: 2,
          name: "Merge intervals",
        },
      ],
    }),
  };
});

exports.handler = async function (event: APIGatewayEvent, context: Context) {
  console.log(process.env);
  const host = process.env.DB_ENDPOINT_ADDRESS;
  try {
    console.log(`host:${host}`);
    const database = process.env.DB_NAME || "";
    const dbSecretArn = process.env.DB_SECRET_ARN || "";
    const secretManager = new SecretsManager({
      region: "ap-south-1",
    });
    console.log({ dbSecretArn });
    const secretParams: SecretsManager.GetSecretValueRequest = {
      SecretId: dbSecretArn,
    };
    const dbSecret = await secretManager.getSecretValue(secretParams).promise();
    const secretString = dbSecret.SecretString || "";
    if (!secretString) {
      throw new Error("secret string is empty");
    }
    const { password } = JSON.parse(secretString);
    console.log({ password });
    const knex = knexConnection({
      client: "pg",
      connection: {
        host,
        database,
        password,
        port: 5432,
      },
    });
  } catch (error) {
    console.log(error);
  }

  //   console.log("success");

  return await api.run(event, context);
};
