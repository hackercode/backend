import * as cdk from "aws-cdk-lib";
import { Runtime } from "aws-cdk-lib/aws-lambda";
import { NodejsFunction } from "aws-cdk-lib/aws-lambda-nodejs";
import { LambdaIntegration, RestApi } from "aws-cdk-lib/aws-apigateway";
import {
  InstanceClass,
  InstanceSize,
  InstanceType,
  Port,
  SecurityGroup,
  SubnetType,
  Vpc,
} from "aws-cdk-lib/aws-ec2";
import {
  Credentials,
  DatabaseInstance,
  DatabaseInstanceEngine,
  PostgresEngineVersion,
} from "aws-cdk-lib/aws-rds";
import { join } from "path";
import { Duration } from "aws-cdk-lib";

export class InfraStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const vpc = new Vpc(this, "VpcRDS", {
      maxAzs: 2,
      subnetConfiguration: [
        {
          cidrMask: 24,
          name: "privateVPC",
          subnetType: SubnetType.PRIVATE_WITH_EGRESS,
        },
        {
          cidrMask: 24,
          name: "publicVPC",
          subnetType: SubnetType.PUBLIC,
        },
      ],
    });

    const api = new RestApi(this, "api", {
      description: "API for hackercode",
      deployOptions: {
        stageName: "dev",
      },
      defaultCorsPreflightOptions: {
        allowHeaders: [
          "Content-Type",
          "X-Amz-Date",
          "Authorization",
          "X-Api-Key",
        ],
        allowMethods: ["OPTIONS", "GET", "POST", "PUT", "PATCH", "DELETE"],
        allowCredentials: true,
        allowOrigins: ["http://localhost:3000"],
      },
    });

    const question = api.root.addResource("question");

    const dbSecurityGroup = new SecurityGroup(this, "RDSSecurityGroup", {
      vpc,
    });

    const databaseName = "hackercode" + process.env.ENV ? "dev" : "";

    const dbInstance = new DatabaseInstance(this, "RDSInstance", {
      engine: DatabaseInstanceEngine.postgres({
        version: PostgresEngineVersion.VER_13,
      }),
      vpc,
      vpcSubnets: vpc.selectSubnets({
        subnetType: SubnetType.PUBLIC,
      }),
      databaseName,
      securityGroups: [dbSecurityGroup],
      credentials: Credentials.fromGeneratedSecret("postgres"),
    });

    const questionLambda = new NodejsFunction(this, "question", {
      entry: join(__dirname, "..", "lambda", "question.ts"),
      runtime: Runtime.NODEJS_16_X,
      handler: "handler",
      vpc,
      timeout: Duration.seconds(20),
      bundling: {
        externalModules: ["pg-native"],
      },
      environment: {
        DB_ENDPOINT_ADDRESS: dbInstance.dbInstanceEndpointAddress,
        DB_NAME: databaseName,
        DB_SECRET_ARN: dbInstance.secret?.secretFullArn || "",
      },
    });

    dbInstance.connections.allowFrom(questionLambda, Port.tcp(5432));

    question.addMethod("GET", new LambdaIntegration(questionLambda));
    question.addMethod("POST", new LambdaIntegration(questionLambda));
    question.addMethod("PUT", new LambdaIntegration(questionLambda));
    question.addMethod("DELETE", new LambdaIntegration(questionLambda));
  }
}
